fetch('http://0.0.0.0:8000/src/public/output/matchesPerYear.json')
.then((response) => response.json())
.then( (matchesPerYear)=> graphForMAtchesPerYear(matchesPerYear) )

function graphForMAtchesPerYear (matchesPerYear){
Highcharts.chart('container1', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Matches per year'
  },
  subtitle: {
    text: 'Source: Kaggle.com'
  },
  xAxis: {
    categories : Object.keys(matchesPerYear),
  },
  yAxis: {
    title: {
      text: 'Matches'
    }
  },
  series: [{
    name: 'Matches',
    data: Object.values(matchesPerYear)
  }]
});
}

fetch('http://0.0.0.0:8000/src/public/output/extraRunsPerTeam.json')
.then((response) => response.json())
.then( (extraRunsPerTeam)=> graphForExtraRunsPerTeam(extraRunsPerTeam) )

function graphForExtraRunsPerTeam(extraRunsPerTeam){
Highcharts.chart('container2', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Extra runs per team'
  },
  subtitle: {
    text: 'Source: Kaggle.com'
  },
  xAxis: {
    categories : Object.keys(extraRunsPerTeam),
  },
  yAxis: {
    title: {
      text: 'Runs'
    }
  },
  series: [{
    name: 'Each',
    data: Object.values(extraRunsPerTeam)
  }]
});
}

fetch('http://0.0.0.0:8000/src/public/output/economicalBowlers.json')
.then((response) => response.json())
.then( (economicalBowlers)=> graphForEconomicalBowlers(economicalBowlers) )

function graphForEconomicalBowlers(economicalBowlers){
  Highcharts.chart('container3', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Top economical bowlers in 2015'
    },
    subtitle: {
      text: 'Source: Kaggle.com'
    },
    xAxis: {
      categories : Object.keys(economicalBowlers),
    },
    yAxis: {
      title: {
        text: 'Economy'
      }
    },
    series: [{
      name: 'Each',
      data: Object.values(economicalBowlers)
    }]
  });
}

fetch('http://0.0.0.0:8000/src/public/output/teamWinningsPerYear.json')
.then((response) => response.json())
.then( (data) => graphForTeamWinningsPerYear(data)
)

function graphForTeamWinningsPerYear(data){
  const years = Object.entries(data).reduce(function (acc,curr) {
    let name = Object.keys(curr[1]).reduce(function (acc2,curr2) {
      if(!acc2.includes(curr2)){
        acc2.push(curr2)
      }
      return acc2;
    },acc)
    return name
  },[]).sort()

  Highcharts.chart('container4', {
    chart: {
      type: 'line'
    },
    title: {
      text: 'Team winnings per year'
    },
    subtitle: {
      text: 'Source: Kaggle.com'
    },
    xAxis: {
      categories : years,
    },
    yAxis: {
      title: {
        text: 'Won matches'
      }
    },
    series: Object.entries(data).map((each) => {

      let name = each[0];
      let yearValuesTeam = Object.keys(each[1])
      let required = years.reduce( (initialValue,currentYear) =>{
  
        if(yearValuesTeam.includes(currentYear)) {
          initialValue.push(each[1][currentYear])
        }
        else{
          initialValue.push(0)
        }
        return initialValue
      },[])
  
      return {'name' : name, "data" :required}
    })
  });
}
