const path = require("path");
const fs = require("fs");

const ipl = require("./ipl");
const matchesFilePath = path.resolve(__dirname,"../data/matches.csv")
const deliveriesFilePath = path.resolve(__dirname,"../data/deliveries.csv")

const csv=require('csvtojson');
const iplTest = async() =>{

    const matchesJson = await csv().fromFile(matchesFilePath).then( (jsonObj)=>{ return jsonObj} )
    const deliveriesJson = await csv().fromFile(deliveriesFilePath).then( (jsonObj)=>{ return jsonObj} )

    let result_1 = ipl.numberOfMatchesInYear(matchesJson);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/matchesPerYear.json"),
        JSON.stringify(result_1,null,2),
        (err) => {if(err) console.log(err)}
    )
    
    let result_2 = ipl.teamWinningsPerYear(matchesJson);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/teamWinningsPerYear.json"),
        JSON.stringify(result_2,null,2),
        (err) =>{if(err) console.log(err)}
    )

    let result_3 = ipl.extraRunsPerTeam(matchesJson,deliveriesJson,2016);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/extraRuns.json"),
        JSON.stringify(result_3,null,2),
        (err) =>{if(err) console.log(err)}
    )

    let result_4 = ipl.economicalBowlers(matchesJson,deliveriesJson,2015);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/economicalBowlers.json"),
        JSON.stringify(result_4,null,2),
        (err) =>{if(err) console.log(err)}
    )

    let extraResult1 = ipl.wonTossAndMatch(matchesJson);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/wonTossAndMatch.json"),
        JSON.stringify(extraResult1,null,2),
        (err) =>{if(err) console.log(err)}
    )
    
    let extraResult2 = ipl.playerOfMatchInSeason(matchesJson);
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/playerOfMatch.json"),
        JSON.stringify(extraResult2,null,2),
        (err)=>{if(err) console.log(err)}
    )
    
    let extraResult3 = ipl.strikeRateOfBatsman(matchesJson,deliveriesJson)
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/strikeRateOfBatsman.json"),
        JSON.stringify(extraResult3,null,2),
        (err)=>{if(err) console.log(err) } 
    )

    let extraResult4 = ipl.onePlayerDismissedByAnother(deliveriesJson)
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/onePlayerDismissedByAnother.json"),
        JSON.stringify(extraResult4,null,2),
        (err)=>{if(err) console.log(err) } 
    )

    let extraResult5 = ipl.bestEconomyBowlerInSuperovers (deliveriesJson)
    fs.writeFileSync(
        path.resolve(__dirname,"../public/output/bestEconomyBowlerInSuperovers.json"),
        JSON.stringify(extraResult5,null,2),
        (err)=>{if(err) console.log(err) } 
    )
}

iplTest()
