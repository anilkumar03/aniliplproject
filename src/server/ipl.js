const numberOfMatchesInYear = (matchesList) => {
    let result = matchesList.reduce(function (acc, match) {
        let year = match.season
        if (!acc[year]) { acc[year] = 1 }
        else { acc[year] += 1 }
        return acc
    }, {})
    return result
}


const teamWinningsPerYear = (matchList) => {
    let result = matchList.reduce(function (acc, match) {
        let team = match.winner
        let year = match.season
        if (!acc[team]) { acc[team] = {} }
        if (!acc[team][year]) { acc[team][year] = 1 }
        acc[team][year] += 1
        return acc
    }, {})
    return result
}

const extraRunsPerTeam = (matches, deliveries, year) => {
    let Ids = {}
    for (let match of matches) {
        Ids[match.id] = match.season
    }

    let result = {}

    for (let delivery of deliveries) {
        if (Ids[delivery.match_id] == year) {
            if (!result[delivery.bowling_team]) {
                result[delivery.bowling_team] = parseFloat(delivery.extra_runs)
            }
            result[delivery.bowling_team] += parseFloat(delivery.extra_runs)
        }
    }
    return result

}

const economicalBowlers = (matches, deliveries, year) => {
    let Ids = {}
    for (let match of matches) {
        Ids[match.id] = match.season
    }

    let result = {}
    for (let delivery of deliveries) {
        if (Ids[delivery.match_id] == year) {

            if (!result[delivery.bowler]) {
                result[delivery.bowler] = { 'legalballs': 0, 'runsConceeded': 0, 'economy': 0 }
            }
            result[delivery.bowler].runsConceeded += parseFloat(delivery.total_runs)
            if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
                result[delivery.bowler].legalballs += 1
            }
            result[delivery.bowler].economy = (result[delivery.bowler].runsConceeded *6 /result[delivery.bowler].legalballs).toFixed(2) ;
        }
    }

    let economies = []
    for (let key in result){
        economies.push(result[key].economy)
    }
    let sortedEconomy = economies.sort((a,b)=> a-b)

    let answer = {}
    for (i=0;i<10;i++){
        economy = sortedEconomy[i]
        for (let key in result){
            if(result[key].economy==economy){
                answer[key] = economy
            }
        }
    }
    return answer
}

const wonTossAndMatch = (matches) => {
    let teamsWonBoth = {}
    for (let match of matches) {
        let Winner = match.winner;
        if (Winner == match.toss_winner) {
            if (!teamsWonBoth[Winner]) {
                teamsWonBoth[Winner] = 1;
            }
            else {
                teamsWonBoth[Winner] += 1
            }
        }
    }
    return teamsWonBoth;
}

const playerOfMatchInSeason = (matches) => {
    let bestPlayersOfYear = {};

    for (const match of matches) {
        let year = match.season;
        if (!bestPlayersOfYear[year]) {
            bestPlayersOfYear[year] = {}
        }
        else {
            let bestPlayer = match.player_of_match
            if (!(bestPlayersOfYear[year][bestPlayer])) {
                bestPlayersOfYear[year][bestPlayer] = 1
            }
            else {
                bestPlayersOfYear[year][bestPlayer] += 1
            }
        }

    }

    let result = {};
    let years = Object.keys(bestPlayersOfYear)

    for (let Y of years) {
        let playersObj = bestPlayersOfYear[Y]
        let playersNames = Object.keys(playersObj)

        let highest = 0
        let player = ""
        for (let Name of playersNames) {
            if (playersObj[Name] > highest) {

                highest = playersObj[Name]
                player = Name
            }
        }
        if (!result[Y]) {
            result[Y] = {};
            result[Y][player] = highest
        }

    }
    return result

}

const strikeRateOfBatsman = (matches, deliveries) => {

    let result = {}
    for (let delivery of deliveries) {
        let player = delivery.batsman
        let run = delivery.batsman_runs
        let ball = delivery.ball
        let strikeRate = 0

        for (match of matches) {
            let year = match.season
            if (!result[player]) {
                result[player] = {}

            }
            if (!result[player][year]) {
                result[player][year] = { "runs": 0, "balls": 0, "strikeRate": 0 }
            }
            if (result[player][year]) {
                result[player][year].runs += parseFloat(run)
                result[player][year].balls += parseFloat(ball)
                result[player][year].strikeRate = parseFloat((result[player][year].runs / result[player][year].balls) * 100).toFixed(4)
            }
        }

    }
    return result

}

const onePlayerDismissedByAnother = (deliveries) => {
    let result = {}
    let dismissalType2 = ['obstructing the field', 'retired hurt', 'run out', '']
    for (const delivery of deliveries) {
        let dismissal = delivery.dismissal_kind
        if (!dismissalType2.includes(dismissal)) {
            if (!result[delivery.player_dismissed]) {
                result[delivery.player_dismissed] = {};
            }
            if (!result[delivery.player_dismissed][delivery.bowler]) {
                result[delivery.player_dismissed][delivery.bowler] = 0;
            }
            result[delivery.player_dismissed][delivery.bowler] += 1;
        }
    }

    for (const key in result) {
        let dismissedValueArray = Object.values(result[key])
        dismissedValueArray.sort(function (a, b) { return b - a })
        let newObj = {}
        for (const subkey in result[key]) {
            if (dismissedValueArray[0] == result[key][subkey]) {
                newObj[subkey] = dismissedValueArray[0]
            }
        }
        result[key] = newObj;
    }
    return result
}

const bestEconomyBowlerInSuperovers = (deliveries) => {
    let result = {}
    if (!deliveries) return {}
    for (const delivery of deliveries) {
        if (delivery.is_super_over == 1) {
            if (!result[delivery.bowler]) {
                result[delivery.bowler] = { 'legal_balls': 0, 'runs_given': 0, 'economy': 0 }
            }
            if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
                result[delivery.bowler]['legal_balls'] += 1;
            }
            result[delivery.bowler]['runs_given'] += parseFloat(delivery.total_runs)
            if (result[delivery.bowler]['legal_balls'] > 0) {
                result[delivery.bowler]['economy'] =
                    (result[delivery.bowler]['runs_given'] * 6 /
                        result[delivery.bowler]['legal_balls']).toFixed(2)
            }
        }
    }

    let bestEconomy = [];
    for (const key in result) {
        bestEconomy.push((result[key]['economy']))
    }
    bestEconomy.sort((a, b) => a - b)
    let bestBowlerInSuperOver = {}
    for (const key in result) {
        if (bestEconomy[0] == result[key]['economy']) {
            bestBowlerInSuperOver[key] = bestEconomy[0];
        }
    }
    return bestBowlerInSuperOver;
}

module.exports.numberOfMatchesInYear = numberOfMatchesInYear;
module.exports.teamWinningsPerYear = teamWinningsPerYear;
module.exports.extraRunsPerTeam = extraRunsPerTeam;
module.exports.economicalBowlers = economicalBowlers;

module.exports.wonTossAndMatch = wonTossAndMatch;
module.exports.playerOfMatchInSeason = playerOfMatchInSeason;
module.exports.strikeRateOfBatsman = strikeRateOfBatsman;
module.exports.onePlayerDismissedByAnother = onePlayerDismissedByAnother
module.exports.bestEconomyBowlerInSuperovers = bestEconomyBowlerInSuperovers